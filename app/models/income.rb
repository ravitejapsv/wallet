class Income < ActiveRecord::Base
  belongs_to :user
  validates :amount, presence: true
  validates :amount, :numericality => { :greater_than_or_equal_to => 0, :message => ' - Income should be a positive numeric value.' }
  validates :limit, :numericality => { :greater_than_or_equal_to => 0, :message => ' - Expenditure should be a positive numeric value.' } 
  validates :limit, :numericality => { :less_than_or_equal_to => :amount, :message => ' - Expenditure limit should be less than Income.' }
end
