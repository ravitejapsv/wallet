class Cost < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  validates :amount, presence: true
  validates :amount, :numericality => { :greater_than_or_equal_to => 0, :message => ' - Expenditure should be a positive numeric value.' }
end
