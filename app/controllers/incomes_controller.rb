class IncomesController < ApplicationController
  before_action :set_income, only: [:edit, :update]
  
  def new
   @income = Income.new
  end

  def edit
  end

  def create
    @income = Income.new(income_params)
    @income.user_id = current_user.id
    respond_to do |format|
      if @income.save
        format.html { redirect_to root_path, notice: 'Income is successfully added.' }
        format.json { render :show, status: :created, location: @income }
      else
        format.html { render :new }
        format.json { render json: @income.errors, status: :unprocessable_entity }
      end
    end
  end
  def update
    respond_to do |format|
      if @income.update(income_params)
        format.html { redirect_to root_path, notice: 'Income is successfully updated.' }
        format.json { render :show, status: :ok, location: @income }
      else
        format.html { render :edit }
        format.json { render json: @income.errors, status: :unprocessable_entity }
      end
    end
  end

private
    def set_income
      @income = Income.find(params[:id])
    end

    def income_params
      params.require(:income).permit(:amount,:limit)
    end
end
