class CostsController < ApplicationController
  before_action :set_cost, only: [:edit, :update]
 
  def index
    @costs = current_user.costs
  end

  def new
    @cost = Cost.new
  end
  
  def edit
  end
 
  def create
    @cost = Cost.new(cost_params)
    @cost.user_id = current_user.id
    @cost.category_id = params[:categories]
    respond_to do |format|
      if @cost.save
        format.html { redirect_to costs_path, notice: 'Expenditure is updated.' }
        format.json { render :show, status: :created, location: @cost }
      else
        format.html { render :new }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    
    def set_cost
      @cost = Cost.find(params[:id])
    end

    def cost_params
      params.require(:cost).permit(:amount, :category_id, :user_id, :description)
    end
end
